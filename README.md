# Feed-Localization Project (Internship)

### What is this repository for? ###

* Experimenting with MobilenetV3+SSDLite in pytorch for Feed-localization task.


### How do I get set up? (for intern) ###

#### login to gcloud instance
**ssh into the instance**
```Shell
$ gcloud beta compute ssh "ml-t4-1" --zone "us-central1-f" --project "virgosvs-staging-233821"
```
**map a port on the instace to local machine**
```Shell
$ gcloud beta compute ssh "ml-t4-1" --zone "us-central1-f" --project "virgosvs-staging-233821" --ssh-flag="-N -L 8082:localhost:8082"
```

#### Dependencies
package dependencies are managed by `conda`
